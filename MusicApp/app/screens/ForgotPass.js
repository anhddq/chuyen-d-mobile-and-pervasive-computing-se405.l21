import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
} from 'react-native';
//import firebaseApp from 'firebase'
import Icon from 'react-native-vector-icons/FontAwesome5';
import RNFetchBlob from 'rn-fetch-blob';
import {Alert} from 'react-native';
import {FirebaseApp} from '../components/FirebaseConfig.js';

//import {firebaseConfig} from "../components/FirebaseConfig"

//import * as Google from 'expo-google-app-auth';

export default class ForgotPassScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderSendCode: true,
      user: '',
      pass: '',
      rePass: '',
    };
  }
  componentDidMount() {
    //this._checkLoginnedIn();
  }
  _checkFileLocal() {
    var fs = RNFetchBlob.fs;
    var path = RNFetchBlob.fs.dirs.SDCardDir + '/DataLocal';
    RNFetchBlob.fs.exists(path).then(value => {
      if (!value) {
        fs.mkdir(path)
          .then(() => {
            fs.mkdir(path + '/PlayList_Local');
            fs.mkdir(path + '/BaiHatVuaNghe');
            fs.mkdir(path + '/Music_Local').then(() => {
              fs.mkdir(path + '/Music_Local/DataMusicLocal');
            });
          })
          .then(() => {
            let objMusicLocalManager = {total_song: 0, items: []};
            fs.createFile(
              path + '/Music_Local/MusicLocalManager.js',
              JSON.stringify(objMusicLocalManager),
              'utf8',
            );

            let objPlayList_Local = {total_list: 0, list: []};
            fs.createFile(
              path + '/PlayList_Local/PlayListManager.js',
              JSON.stringify(objPlayList_Local),
              'utf8',
            );

            let objBHVuaNghe = {items: []};
            fs.createFile(
              path + '/BaiHatVuaNghe/BaiHatVuaNghe.js',
              JSON.stringify(objBHVuaNghe),
              'utf8',
            );
          });
      }
    });
  }
  _clearAll() {
    this.setState({user: '', pass: '', rePass: ''});
  }
  _renderButton() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'flex-start',
          marginBottom: 0,
        }}>
        <TouchableOpacity
          onPress={() => {
            if (this.state.renderSendCode == true) {
              this._Login();
            } else {
              this.setState({renderSendCode: true});
              this._clearAll();
            }
          }}>
          <View
            style={
              this.state.renderSendCode == true
                ? styles.conButon1
                : styles.conButon2
            }>
            <Text> Đặt lại mật khẩu</Text>
          </View>
        </TouchableOpacity>

        {/* <TouchableOpacity
          onPress={() => {
            if (this.state.renderSendCode == false) {
              this._Register();
            } else {
              this.setState({renderSendCode: false});
              this._clearAll();
            }
          }}>
          <View style={this.state.renderSendCode==false? styles.conButon1:styles.conButon2}>
        <Text> {"Đặt lại mật khẩu"}</Text>
          </View>
        </TouchableOpacity> */}
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Login')}>
          <Icon name="sign-out-alt" size={50} />
        </TouchableOpacity>
      </View>
    );
  }
  _Register() {
    // if(this.state.pass.length<6)
    // {
    //     Alert.alert("Pass must >6")
    //     //return;
    if (this.state.pass != this.state.rePass) {
      Alert.alert('2 mật khẩu phải giống nhau!');
      return;
    }

    FirebaseApp.auth()
      .createUserWithEmailAndPassword(this.state.user, this.state.pass)
      .then(() => {
        Alert.alert('Đăng kí thành công!');
        this.setState({renderSendCode: true});
      })
      .catch(function(error) {
        // Handle Errors here.

        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage + ' code: ' + errorCode);
        if (errorCode == 'auth/email-already-in-use') {
          Alert.alert('E-mail này đã được đăng kí');
          return;
        }

        if (errorCode == 'auth/invalid-email') {
          Alert.alert('E-mail không hợp lệ!');
          return;
        }

        if (errorCode == 'auth/weak-password') {
          Alert.alert('Mật khẩu phải nhiều hơn 5 kí tự!');
          return;
        }

        // ...
      });
  }

  _Login() {
    // if (this.state.user == '') {
    //   Alert.alert('Email null');
    //   return;
    // }
    // if (this.state.pass.length < 6) {
    //   Alert.alert('Pass must >6');
    //   return;

    // }

    FirebaseApp.auth()
      .sendPasswordResetEmail(this.state.user)
      .then(function() {
        Alert.alert(
          'Đã gửi đặt lại mật khẩu đến email, kiểm tra email để đặt lại mật khẩu !! ',
        );
      })
      .catch(function(error) {
        // Alert.alert(error.message);
        var errorCode = error.code;
        console.log(error.message + 'code: ' + error.code);
        if (errorCode == 'auth/invalid-email') {
          Alert.alert('E-mail không hợp lệ');
          return;
        }

        if (errorCode == 'auth/user-not-found') {
          Alert.alert('E-mail này chưa đăng kí!');
          return;
        }
      });

    // FirebaseApp.auth()
    //   .signInWithEmailAndPassword(this.state.user, this.state.pass)
    //   .then(() => {
    //     this.props.navigation.navigate('Home');
    //   })
    //   .catch(function(error) {
    //     // Handle Errors here.
    //     //.alert('Login failed!');
    //     var errorCode = error.code;
    //     var errorMessage = error.message;
    //     console.log(errorMessage + ' code: ' + errorCode);
    //     if (errorCode == 'auth/invalid-email') {
    //       Alert.alert('E-mail không hợp lệ');
    //       return;
    //     }
    //     if (errorCode == 'auth/wrong-password') {
    //       Alert.alert('Sai mật khẩu!');
    //       return;
    //     }
    //     // ...
    //   });
  }

  _renderRepass() {
    return (
      <View>
        <View style={styles.conLogin}>
          <Text> </Text>
          <Icon name="lock" size={35} color="#000" />

          <Text> </Text>
          <TextInput
            secureTextEntry={true}
            value={this.state.pass}
            placeholder=" Mật khẩu"
            style={styles.conInput}
            onChangeText={text => {
              this.setState({pass: text});
            }}
          />
        </View>
        <Text style={{fontSize: 3}} />

        <View style={styles.conLogin}>
          <Text> </Text>
          <Icon name="lock" size={35} color="#000" />

          <Text> </Text>
          <TextInput
            secureTextEntry={true}
            value={this.state.rePass}
            placeholder=" Nhập lại mật khẩu"
            style={styles.conInput}
            onChangeText={text => {
              this.setState({rePass: text});
            }}
          />
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.logo}
            source={{
              uri:
                'https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Muse_Music_App_Logo.png/900px-Muse_Music_App_Logo.png',
            }}
          />
          <Text style={styles.title}>Quên mật khẩu</Text>
        </View>
        <View style={styles.footer}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'flex-start',
              flex: 1,
              marginTop: 75,
            }}>
            <View style={styles.conLogin}>
              <Text> </Text>
              <Icon name="user" size={35} style={styles.icon} color="#000" />

              <Text> </Text>
              <TextInput
                value={this.state.user}
                placeholder="  E-mail"
                style={styles.conInput}
                onChangeText={text => {
                  this.setState({user: text});
                }}
              />
            </View>
            <View style={{height: 20}} />
            {this._renderButton()}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#FFB6C1'},
  header: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  footer: {
    flex: 2,
    backgroundColor: '#fff',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold',
  },
  icon: {
    marginTop: 5,
    marginLeft: 5,
  },
  logo: {
    width: 150,
    height: 150,
    resizeMode: 'cover',
    borderRadius: 39,
    justifyContent: 'center',
    alignItems: 'center',
  },
  conLogin: {
    flexDirection: 'row',
    //justifyContent: 'center',
    borderWidth: 2,
    backgroundColor: 'transparent',
    borderRadius: 10,
  },
  conInput: {
    width: 300,
    backgroundColor: 'transparent',
    marginBottom: 0,
    borderRadius: 20,
  },
  conButon1: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1589FF',
    width: 180,
    height: 50,
    borderWidth: 2,
    borderColor: '#555',
    marginBottom: 5,
    borderRadius: 25,
    margin: 3,
  },
  conButon2: {
    marginTop: 10,
    marginLeft: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#64E986',
    width: 150,
    height: 50,
    borderWidth: 2,
    borderColor: '#555',
    marginBottom: 5,
    borderRadius: 25,
    margin: 3,
  },
});
