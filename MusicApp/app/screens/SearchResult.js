import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import {Dimensions} from 'react-native';
import DanhSachBaiHat from '../components/DanhSachBaiHat';
const screenWidth = Math.round(Dimensions.get('window').width);
class SearchResultScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataSearch: [],
    };
  }
  componentDidMount() {
    this._loadDataSearch(this.props.navigation.getParam('data'));
  }
  _loadDataSearch(value) {
    if (value === '') {
      this.setState({dataSearch: []});
    } else {
      fetch(
        'https://zingmp3.vn/api/search?type=song&q=' +
          value +
          '&start=0&count=20&ctime=1575083405&sig=4e2f8c458e8fe8516223757a0234ff84e6ea1381bfa7e242b69d3506b71b9d2becce29fec1ca25370fc6b3e1d2958f8c95bd8da5ac96951b73121105e0afbfea&api_key=38e8643fb0dc04e8d65b99994d3dafff',
      )
        .then(response => response.json())
        .then(response => {
          this.setState({
            dataSearch: response.data.items,
          });
        });
    }
  }
  render() {
    return (
      <View style={styles.container1}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Home')}>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Icon
              name={'arrow-left'}
              color={'#341f97'}
              size={30}
              style={{marginTop: 25}}
            />
            <Text style={styles.tieuDe}> Kết quả tìm kiếm</Text>
          </View>
        </TouchableOpacity>
        <DanhSachBaiHat
          kind="download"
          dataDanhSachBaiHat={this.state.dataSearch}
        />
      </View>
    );
  }
}
export default SearchResultScreen;
const styles = StyleSheet.create({
  container1: {
    flex: 1,
    width: screenWidth * 0.95,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'column',
    marginTop: 1,
    borderRadius: 10,
    //alignItems: 'center',
    backgroundColor: '#ffffffcc',
  },
  tieuDe: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    fontSize: 20,
    fontFamily: 'vincHand',
    fontWeight: 'bold',
    marginTop: 25,
    marginLeft: 3,
    marginBottom: 3,
    color: '#000',
  },
});
