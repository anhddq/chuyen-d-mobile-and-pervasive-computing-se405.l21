import React, {Component} from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import HomeScreen from './screens/Home';
import LoginScreen from './screens/Login';
import StreamScreen from './screens/Stream';
import SearchScreen from './screens/Search';
import SearchResultScreen from './screens/SearchResult';
import ProfileScreen from './screens/Profile';
import ChiTiet_PlayListOnlineScreen from './screens/ChiTiet_PlayListOnlineScreen';
import ChiTiet_PlayListOfflineScreen from './screens/ChiTiet_PlayListOffline';
import LibraryScreen from './screens/Library';
import ForgotPassScreen from './screens/ForgotPass';
import imagplaylist from './screens/imagplaylist';
import Icon_ from 'react-native-vector-icons/FontAwesome5';

import store from './redux/store';
import {Provider} from 'react-redux';

//#region Stack
const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    ChiTiet_PlayListOnline: ChiTiet_PlayListOnlineScreen,
  },
  {
    //headerMode: 'non',
  },
);
const SearchStack = createStackNavigator({
  Search: SearchScreen,
});
//
const StreamStack = createStackNavigator({
  Stream: StreamScreen,
  // Song: SongScreen,
});
const LibraryStack = createStackNavigator({
  Library: LibraryScreen,
  ChiTiet_PlayListOffline: ChiTiet_PlayListOfflineScreen,
  // Song: SongScreen,
});
const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
  // Song: SongScreen,
});
//#endregion

//createBottomTabNavigator(RouteConfigs, TabNavigatorConfig);
const Tabs = createBottomTabNavigator(
  {
    Home: HomeStack,
    //Search: SearchStack,
    Stream: StreamStack,
    Library: LibraryStack,
    Profile: ProfileStack,
  },
  {
    defaultNavigationOptions: ({navigation}) => {
      const {routeName} = navigation.state;
      let tile = '';
      let iconName = 'home';

      switch (routeName) {
        case 'Home':
          iconName = 'home';
          tile = 'Trang chủ';
          break;
        case 'Search':
          iconName = 'search';
          tile = 'Tìm kiếm';
          break;
        case 'Stream':
          iconName = 'headphones-alt';
          tile = 'Đang phát';
          break;
        case 'Profile':
          iconName = 'id-card';
          tile = 'Cá nhân';
          break;
        case 'Library':
          iconName = 'list';
          tile = 'Thư viện';
          break;
        default:
          break;
      }
      return {
        tabBarIcon: ({tintColor}) => {
          return (
            <Icon_
              name={iconName}
              size={25}
              color={tintColor}
              style={{marginTop: 5, marginRight: 3}}
            />
          );
        },

        tabBarOptions: {
          activeTintColor: '#341f97',
          inactiveTintColor: '#fff',
          activeBackgroundColor: '#FFB6C1',
          inactiveBackgroundColor: '#FFB6C1',
          borderColor: '#000',
        },
        title: tile,
      };
    },
  },
);

const LoginStack = createStackNavigator({
  Login: LoginScreen,
  ForgotPass: ForgotPassScreen,

  // Song: SongScreen,
});

const RootStack = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        header: null,
      },
    },

    ForgotPass: {
      screen: ForgotPassScreen,
      navigationOptions: {
        header: 'Quên mật khẩu',
      },
    },
    SearchResult: {
      screen: SearchResultScreen,
      navigationOptions: {
        header: null,
      },
    },
    imagplaylist: {
      screen: imagplaylist,
      navigationOptions: {
        header: null,
      },
    },
    Home: {
      screen: Tabs,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'Login',
    headerMode: 'non',
  },
);

const AppContainer = createAppContainer(RootStack);
export default class App extends Component {
  componentDidMount() {}

  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
